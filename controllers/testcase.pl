#!/usr/bin/perl -wT

=head1 NAME

testcase.pl - A simple Test Case Controller for the MVC demo.

=head1 SYNOPSIS

    perl -T testcase.pl view=simple id=2
    perl -T testcase.pl view=nice id=3
    
    # not implemented; you should try it..
    perl -T testcase.pl view=list

=head1 DESCRIPTION

This script represents the B<Controller> within the MVB<C> design. The
B<Controller> is responsible for generating a response for a request. It
will take data from the B<Model> and map it to specific places within the
B<Views> (aka: Templates). B<Models> and B<Views> remain B<decoupled>
and unaware of each other!

=head1 AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=head1 COPYRIGHT

Copyright (C) 2012  Raymond L. Rivera

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.

=cut


use lib '../models';

use strict;
use warnings;

#
# models
#
use WBS::TestCase;


#
# globals
#
my $TEMPLATES   = {
    simple  => "../views/testcase_simple.html",
    nice    => "../views/testcase_nice.html",
    list    => "../views/testcase_list.html"    # TODO: Excercise for reader ;)
};



#
# Normally you'd use a module, but for the purpose of this demo..
# I'm assuming always valid inputs, etc..
#
my %get     = (
    view    => (split '=', $ARGV[0])[1] || undef,
    id      => (split '=', $ARGV[1])[1] || undef
);

if( !defined $get{view} ) {
    print "Usage: perl -T testcase.pl {view=viewname} [id=idNum]\n";
    exit 1;
}


#
# Check which view we need to render
#
if( $get{view} eq "list" ) {
    print list_view();
} else {
    print detail_view({
        view    => $get{view},
        id      => $get{id}
    });
}


=head1 FUNCTIONS

=head2 detail_view  -- Renders a view designed for a single Test Case.

=over

=item SYNOPSIS

    my $html = detail_view({
                    view    => $get{view},
                    id      => $get{id}
                });

=item INPUT

    $args->{view}   -- The type of view that the controller should use.
    $args->{id}     -- The ID of the Test Case to be requested from the model.

=item OUTPUT

    $html   -- The HTML code to be sent to the client browser.

=item DESCRIPTION

This function handles requests for detail-type views and generates valid
HTML documents for them.

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=back

=cut

sub detail_view {
    my $args    = shift;
    my $html    = _load_template( $args->{view} );
    my $case    = WBS::TestCase->new({
                    loadFromDisk    => 1,
                    id              => $args->{id}
                });
    
    # g = globally, e = eval, i = case insensitive
    $html =~ s/{% tc.id %}/$case->id()/gie;
    $html =~ s/{% tc.name %}/$case->name()/gie;
    $html =~ s/{% tc.desc %}/$case->desc()/gie;
    $html =~ s/{% tc.script %}/$case->script()/gie;
    
    return $html;
}


=head2 list_view  -- Renders a view designed for a list of Test Cases.

=over

=item SYNOPSIS

    my $html = list_view();

=item INPUT

None.

=item OUTPUT

    $html   -- The HTML code to be sent to the client browser.

=item DESCRIPTION

This function handles requests for a list-type view and generates a valid
HTML document for it. B<Not Yet Implemented>

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=back

=cut

sub list_view {
    my $html    = _load_template( "list" );
    my @cases   = WBS::TestCase::list();
    
    return "INFO: Not Yet Implemented\n";
    
    # TODO: You can fill out the specified 'list' template and code this part.
    #       The special code to handle loop-blocks within views/templates must NOT
    #       go inside a controller. It must go inside its own lib (e.g. Template::Manager)
}


=head1 HELPER FUNCTIONS

=head2 _load_template  -- Loads a template corresponding to a specific view.

=over

=item SYNOPSIS

    my $html = _load_template( $view );

=item INPUT

    $view   -- The path to a specific template file including the filename.

=item OUTPUT

    $html   -- The HTML code to be sent to the client browser.

=item DESCRIPTION

This function loads and returns a raw template and returns it for processing.

=item LIMITATIONS

The correct place to put this would be in a utilty module/lib, not in the
Controller itself, much less in the Model. However, the purpose of this demo
is to explain the basics, not to continue development..

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=back

=cut

sub _load_template {
    my $view = shift;
    my $path = $TEMPLATES->{$view};
    
    open my $fh, $path || die $!;
    my @lines = <$fh>;
    close $fh;
    return join('', @lines);
}
