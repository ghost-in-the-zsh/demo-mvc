#!/usr/bin/perl -wT

=head1 NAME

WBS::TestCase.pm - A simple Test Case module for the MVC demo.

=head1 SYNOPSIS
    
    use WBS::TestCase;
    
    # Create a new test case
    my $case    =   WBS::TestCase->new({
                        name    => "Boot Linux",
                        script  => "tests/boot/linux.pl"
                    });
    
    # Get/Set some attribute info
    my $id      =   $case->id();    # undef until saved!
    my $name    =   $case->name();
    my $desc    =   $case->desc("This test case boots to the Linux shell.");
    
    # Save a test case to whatever storage location is being used
    my $saved   =   $case->save();
    if($saved) {
        # ...
    } else {
        # ...
    }
    
    # Load specific test case from SAVED data; undef (or crash!) if non-existent ID
    my $casecpy =   WBS::TestCase->new({
                        loadFromDisk    => 1,   # we want to load an existing TC..
                        id              => 3    # ..with this ID
                    });
    
    # ...
    
    my @cases = WBS::TestCase::list();
    print $_->name()."\n" foreach(@cases);

=head1 DESCRIPTION

This module represents the B<Model> in the B<M>VC design. The B<Model> is
responsible for managing (e.g. accessing, storing, etc.) B<data>. Clients
of this B<Model> (i.e. B<Controllers>) don't know (or care!) where the data is
stored. The data could be stored in a database or just a plain file. It could
be in the local file system or even at a remote location!

=head1 AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=head1 COPYRIGHT

Copyright (C) 2012  Raymond L. Rivera

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.

=cut

package WBS::TestCase;

use strict;
use warnings;
use Text::CSV;  # our database will be a csv text file; feel free to change it


=head1 EXPORTED CONSTANTS

=head2 $VERSION

Specifies the version number of this module.

=cut

our $VERSION    = '1.00';


#
# Global const.
#
my $DATABASE    = '../data/testcase.data';



=head1 PUBLIC METHODS

=head2 new  -- Constructs a new object of type WBS::TestCase.

=over

=item SYNOPSIS
    
    # create a new default TC
    my $tc1 = WBS::TestCase->new();
    
    # create and initialize a new TC with specific data
    my $tc2 = WBS::TestCase->new({
                id      => 2
                name    => "The name",
                desc    => "The description here..",
                script  => "path/to/script.pl"
            });
    
    # load the specified TC from the storage location
    my $tc3 = WBS::TestCase->new({
                id              => 3,
                loadFromDisk    => 1    # default is 'no'
            });

=item INPUT

    $args->{id}             -- The ID of the Test Case.
    $args->{name}           -- The name of the Test Case.
    $args->{desc}           -- The description of the Test Case.
    $args->{script}         -- The path to the script that automates the Test Case.
    $args->{loadFromDisk}   -- A flag that specifies whether to try searching
                               for a pre-existing Test Case or not (default 'false').

=item OUTPUT

    $TestCaseObj    -- An object of type WBS::TestCase

=item DESCRIPTION

This method handles the creation and initialization of a new object. If specified, it will
load an existing TC from disk.

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=back

=cut

sub new {
    my($class, $args) = @_;
    
    my $self = bless({}, $class);
    
    if($args->{loadFromDisk}) {
        $self = $self->_load($args->{id});
    } else {
        $self->{id}     = exists $args->{id}      ? $args->{id}     : undef;
        $self->{name}   = exists $args->{name}    ? $args->{name}   : "Rename Me";
        $self->{desc}   = exists $args->{desc}    ? $args->{desc}   : "No Description";
        $self->{script} = exists $args->{script}  ? $args->{script} : "";
    }
    return $self;
}


=head2 id  -- Gets or Sets the ID.

=over

=item SYNOPSIS
    
    # get the ID of a TC
    my $id = $tc1->id();

    # set the ID for a new TC
    $tc2->id( 4 );

=item INPUT

    $id -- The ID of the Test Case.

=item OUTPUT

    $id -- The ID of the Test Case.

=item DESCRIPTION

Gets or, optionally, sets the ID of a Test Case.

=item LIMITATIONS

For the purposes of this demo, there's no validation. It's assumed to always
be valid and unique. Normally, should be read-only and set only after getting
save()'ed

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=back

=cut

sub id {
    my $self = shift;
    
    $self->{id} = shift if(@_);
    return $self->{id};
}


=head2 name  -- Gets or Sets the name.

=over

=item SYNOPSIS
    
    # get the name of a TC
    my $name = $tc1->name();

    # set the name of a TC
    $tc2->name( "The new name" );

=item INPUT

    $name -- The name of the Test Case.

=item OUTPUT

    $name -- The name of the Test Case.

=item DESCRIPTION

Gets or, optionally, sets the name of a Test Case.

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=back

=cut

sub name {
    my $self = shift;
    
    $self->{name} = shift if(@_);
    return $self->{name};
}


=head2 desc  -- Gets or Sets the description.

=over

=item SYNOPSIS
    
    # get the description of a TC
    my $desc = $tc1->desc();

    # set the description of a TC
    $tc2->desc( "The new description" );

=item INPUT

    $desc -- The description of the Test Case.

=item OUTPUT

    $desc -- The description of the Test Case.

=item DESCRIPTION

Gets or, optionally, sets the description of a Test Case.

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=back

=cut

sub desc {
    my $self = shift;
    
    $self->{desc} = shift if(@_);
    return $self->{desc};
}


=head2 script  -- Gets or Sets the script.

=over

=item SYNOPSIS
    
    # get the script of a TC
    my $desc = $tc1->script();

    # set the script of a TC
    $tc2->script( "new/path/to/script.pl" );

=item INPUT

    $script -- The script of the Test Case.

=item OUTPUT

    $script -- The script of the Test Case.

=item DESCRIPTION

Gets or, optionally, sets the script of a Test Case.

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=back

=cut

sub script {
    my $self = shift;
    
    $self->{script} = shift if(@_);
    return $self->{script};
}


=head2 save  -- Saves the object's data to permanent storage.

=over

=item SYNOPSIS
    
    my $result = $tc1->save();

=item INPUT

None.

=item OUTPUT

    $result -- A value to notify sucess or failure for this operation.

=item DESCRIPTION

Saves the object's data to permanent storage.

=item LIMITATIONS

The result value should be a named constant from some other module that has different
error codes defined, but this is outside the scope of the basic goal for this demo.

B<Not Yet Implemented>

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=back

=cut

sub save {
    my $self = shift;
    # TODO: This is left as an excercise for the reader.
    # TODO: Remember to assign an ID to TC after saving, if it's a new one.
}


=head2 list  -- Gets a list of all Test Case objects.

=over

=item SYNOPSIS
    
    my @testcases = WBS::TestCase::list();
    
    # do something..
    print $_->name() foreach(@testcases);

=item INPUT

None

=item OUTPUT

    @cases -- A list of test case objects loaded from disk.

=item DESCRIPTION

Gets a list of blessed Test Case objects.

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=item LIMITATIONS

The method can be easily modified to work with other storage locations.

=back

=cut

sub list {
    my $self = shift;
    my $csv  = Text::CSV->new();
    
    # Since this is a small demo, just read the
    # full file..
    my @cases;
    open my $fh, $DATABASE || die $!;
    while( my $row = $csv->getline($fh) ) {
        my $case =  WBS::TestCase->new({
                        id      => $row->[0],
                        name    => $row->[1],
                        desc    => $row->[2],
                        script  => $row->[3]
                    });
        push @cases, $case;
    }
    $csv->eof();
    close $fh;
    return @cases;
}


=head2 tostring  -- Creates a string representation of the Test Case.

=over

=item SYNOPSIS
    
    print $case->tostring();

=item INPUT

None.

=item OUTPUT

    $str -- The string representation of the Test Case.

=item DESCRIPTION

Creates a string representation of the Test Case for display based on defined fields.

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=back

=cut

sub tostring {
    my $self = shift;
    
    my $str = "";
    $str .= "ID:      ". ($self->id()     ? $self->id()     : "<undef>")."\n";
    $str .= "Name:    ". ($self->name()   ? $self->name()   : "<undef>")."\n";
    $str .= "Desc:    ". ($self->desc()   ? $self->desc()   : "<undef>")."\n";
    $str .= "Script:  ". ($self->script() ? $self->script() : "<undef>")."\n";
}


=head1 PRIVATE METHODS

=head2 _load  -- Loads the TC with specified ID.

=over

=item SYNOPSIS
    
    

=item INPUT

    $id -- The description of the Test Case.

=item OUTPUT

    $TestCaseObj -- The description of the Test Case.

=item DESCRIPTION

Loads the TC with specified ID from permament storage if it exists.

=item AUTHOR

Raymond L. Rivera   ray.l.rivera [at] gmail [dot] com

=item LIMITATIONS

Assumes that the provided ID exists and is valid.

Currently inefficient because it will load the full list every single time and
then select the correct one, if the ID was valid. Since this demo has a very
small number of entries, then this is negligible at the moment..

=back

=cut

sub _load {
    my $self = shift;
    
    # notice that our IDs are simply line-numbers;
    # guaranteed unique! :)
    my $id  = shift;
    
    # there's also no need for validation in our small demo;
    # it's not the focus.. so I assume all IDs are valid and exist;
    return ( $self->list() )[$id-1];
}

1;
