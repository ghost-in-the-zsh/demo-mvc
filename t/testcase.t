#!/usr/bin/perl -wT

=head1 NAME

testcase.t  -- A simple/minimalistic/hacked set of test cases for my WBS::TestCase module.

=head1 SYNOPSIS

    perl -T testcase.t

=head1 DESCRIPTION

Just run it will you?.. If they fail, make sure you have the required module(s) installed.

=cut

use lib '../models';

use strict;
use warnings;

use Test::More qw( no_plan );

# Confirm requirements are installed in host
use_ok('Text::CSV');
require_ok('Text::CSV');

use_ok('WBS::TestCase');
require_ok('WBS::TestCase');



#
# Now to the meat of the matter
#
use WBS::TestCase;

my $count = scalar WBS::TestCase::list();
is $count, 5, "Number of test cases match";


my $c1 = WBS::TestCase->new();
isa_ok($c1, 'WBS::TestCase');

# defaults
is $c1->id(), undef, "Default undef ID match";
is $c1->name(), "Rename Me", "Default name match";
is $c1->desc(), "No Description", "Default desc match";
is $c1->script(), "", "Default empty script match";


# set and verify
$c1->id(1);
$c1->name("Test Name");
$c1->desc("Test Desc");
$c1->script("path/to/script.pl");
is $c1->id(), 1, "Set id match";
is $c1->name(), "Test Name", "Set name match";
is $c1->desc(), "Test Desc", "Set desc match";
is $c1->script(), "path/to/script.pl", "Set script match";


# load and verify
my $c2 = WBS::TestCase->new({ loadFromDisk => 1, id => 2 });
isa_ok($c2, 'WBS::TestCase');

is $c2->id(), 2, "Loaded ID match";
is $c2->name(), "Boot Linux", "Loaded name match";
is $c2->desc(), "This test case boots to the Linux shell.", "Loaded desc match";
is $c2->script(), "tests/boot/linux.pl", "Loaded script match";


# init and verify
my $c3 = WBS::TestCase->new({ id => 3, script => "spaces.pl" });
isa_ok($c3, 'WBS::TestCase');

is $c3->id(), 3, "Init ID match";
is $c3->name(), "Rename Me", "Init name match";
is $c3->desc(), "No Description", "Init desc match";
is $c3->script(), "spaces.pl", "Init script match";


# load, set, and verify
my $c4 = WBS::TestCase->new({ loadFromDisk => 1, id => 4 });
isa_ok($c4, 'WBS::TestCase');

is $c4->id(), 4, "Init ID match";
$c4->name("Other Name");
$c4->desc("Other Desc");
$c4->script("other_script.pl");
is $c4->name(), "Other Name", "Set name match";
is $c4->desc(), "Other Desc", "Set desc match";
is $c4->script(), "other_script.pl", "Set script match";
